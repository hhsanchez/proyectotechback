FROM node:boron
WORKDIR /miapp
ADD . /miapp
RUN npm install
EXPOSE 3000
CMD ["npm","start"]
